from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket
from lector import lector
import json
class HandleSocket(WebSocket):

    def handleMessage(self):
        # echo message back to client
        # self.msg = json.decode(self.data)
        msg = json.loads(self.data)
        print(msg)
        cmd = msg['cmd']
        if(cmd == 'echo'):
            resp = msg
        elif(cmd == 'ready'):
            resp = lector.ready()
        elif(cmd == 'alta'):
            resp = lector.alta(msg['data'])
        else:
            resp = msg

        self.sendMessage(json.dumps(resp))

    def handleConnected(self):
        print(self.address, 'connected')

    def handleClose(self):
        print(self.address, 'closed')


server = SimpleWebSocketServer('', 8000, HandleSocket)
server.serveforever()
