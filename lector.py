import time
import win32com.client
import win32api
import win32event
import json
import pythoncom
from win32com.client import constants
def WaitWhileProcessingMessages(event, timeout = 20):
    start = time.clock()
    while True:
        # Wake 4 times a second - we can't just specify the
        # full timeout here, as then it would reset for every
        # message we process.
        rc = win32event.MsgWaitForMultipleObjects( (event,), 0,
            250,
            win32event.QS_ALLEVENTS)
        if rc == win32event.WAIT_OBJECT_0:
            # event signalled - stop now!
            return True
        if (time.clock() - start) > timeout:
            # Timeout expired.
            return False
        # must be a message.
        pythoncom.PumpWaitingMessages()

class Lector():
    global relojOCX
    def __init__(self):
        print('init lector')
    def setLector(self, lector):
        self.lector = lector
        print('initEngine %d' % self.lector.InitEngine())
    def reset(self):
        print('reseting lector')
    def alta(self, user):
        time.sleep(2)
        test = True
        if(test):
            #todo ok
            return {'cmd':'alta', 'data':{'status':'ok'}}
        else:
            #algún error
            return {'cmd':'alta', 'data':{'status':'error'}}
    def test(self):
        print(self.lector)
        print('lector')
        return {'cmd':'test', 'data':{'ready': self.lector.Active}}
    def initEngine(self):
        #print(self.lector.Active)
        #if(self.lector.Active):
        #    return {'cmd':'initEngine', 'data':{'ready': True}}
        error = self.lector.InitEngine()
        print('InitEngine: %d' % error)
        return {'cmd':'initEngine', 'data':{'ready': error == 0}}
    def ready(self):
        return {'cmd':'ready', 'data':{'ready': self.lector.Active}}
    def enroll(self):
        self.lector.EnrollCount = 1
        self.lector.CancelEnroll()
        self.lector.BeginEnroll()
        if not WaitWhileProcessingMessages(self.lector.event):
            print("Document load event FAILED to fire!!!")
        print('ok')
        return {'cmd':'enroll', 'data':{'ready': self.lector.Active}}
lector = Lector()
