from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket
from lector import lector
import win32com.client
import win32api
import win32event
import json
import pythoncom
from win32com.client import constants
defaultNamedNotOptArg = pythoncom.Empty
global relojOCX
class OCXEvents:
    global relojOCX
    def __init__(self):
        self.event = win32event.CreateEvent(None, 0, 0, None)
        print("llamo a Init de OCXEvents")
    def OnFingerLeaving(self):
        thread = win32api.GetCurrentThreadId()
        print("OnFingerLeaving event processed on thread %d"%thread)
        # Set the event our main thread is waiting on.
        # win32event.SetEvent(self.event)
    def OnFingerTouching(self):
        thread = win32api.GetCurrentThreadId()
        print("OnFingerTouching event processed on thread %d"%thread)
    def OnEnroll(self, ActionResult=defaultNamedNotOptArg, ATemplate=defaultNamedNotOptArg):
        thread = win32api.GetCurrentThreadId()
        if (ActionResult):
            print("Registro completo")
        else:
            print("Error de registro")
        print("OnEnroll event processed on thread %d"%thread)

class HandleSocket(WebSocket):
    global relojOCX
    def handleMessage(self):
        # echo message back to client
        # self.msg = json.decode(self.data)
        msg = json.loads(self.data)
        print(msg)
        cmd = msg['cmd']
        if(cmd == 'echo'):
            resp = msg
        elif(cmd == 'initEngine'):
            resp = lector.initEngine()
        elif(cmd == 'ready'):
            resp = lector.ready()
        elif(cmd == 'enroll'):
            resp = lector.enroll()
        else:
            resp = msg

        self.sendMessage(json.dumps(resp))

    def handleConnected(self):
        print(self.address, 'connected')

    def handleClose(self):
        print(self.address, 'closed')

relojOCX = win32com.client.DispatchWithEvents("ZKFPEngXControl.ZKFPEngX", OCXEvents)
lector.setLector(relojOCX)
server = SimpleWebSocketServer('', 8000, HandleSocket)
relojOCX.server = server
server.serveforever()
